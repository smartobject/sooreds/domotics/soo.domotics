package com.soo.domotics

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import android.content.Intent
import android.widget.Button
import android.widget.TextView

import kotlinx.android.synthetic.main.activity_main.*

var TAG = "SOO.domotics"
var CONNEXIONSTATE = 0 // 0 = No connexion // 1 = Connected via BT // 2 = Connected via Wi-Fi

class MainActivity : AppCompatActivity() {
    private var TAG = "SOO.domotics"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnSOOblindsAct : Button = findViewById(R.id.btn_SOOblindsAct)
        btnSOOblindsAct.setOnClickListener {
            val intent = Intent(this, SOOBlindsActivity :: class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        }

        val btnSOOoutdoorAct : Button = findViewById(R.id.btn_SOOoutdoorAct)
        btnSOOoutdoorAct.setOnClickListener {
            val intent = Intent(this, SOOOutdoorActivity :: class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        }

        val btnSOOnetAct : Button = findViewById(R.id.btn_SOOnetAct)
        btnSOOnetAct.setOnClickListener {
            val intent = Intent(this, SOONetWifi :: class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        }

        val btn_gotoBT : Button = findViewById(R.id.btn_gotoBT)
        btn_gotoBT.setOnClickListener {
            val intent = Intent(this, SOONetActivity :: class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        }

        connexionstate_textview.text = resources.getStringArray(R.array.sooConnexionState_array)[CONNEXIONSTATE]

    }

    override fun onResume() {
        super.onResume()
        connexionstate_textview.text = resources.getStringArray(R.array.sooConnexionState_array)[CONNEXIONSTATE]
    }
}
