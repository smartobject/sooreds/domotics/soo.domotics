package com.soo.domotics

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.*

import kotlinx.android.synthetic.main.activity_soonet_wifi.*
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.app.ProgressDialog

import android.net.wifi.p2p.WifiP2pManager
import android.net.wifi.WifiManager


//import android.net.wifi.p2p.WifiP2pDevice;

private val REQUEST_ENABLE_WIFI = 2

val m_myWifiComm = WifiCommManager()
class SOONetWifi : AppCompatActivity() {
    private var TAG = "SOO.NetWifi"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_soonet_wifi)
        supportActionBar?.title = "SOO.net: Wi-fi"
        //supportActionBar?.hide()

        //val enableWifiIntent = Intent()
        //startActivityForResult(enableWifiIntent, REQUEST_ENABLE_WIFI)
        //val wifi = getSystemService(Context.WIFI_SERVICE) as WifiManager
        //wifi.isWifiEnabled = true

        btn_gotoBT.setOnClickListener {
            val intent = Intent(this, SOONetActivity :: class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent) }

        // Indicates a change in the Wi-Fi P2P status.
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION)

        // TODO uese these to manage wifi connected feedback
        // Indicates a change in the list of available peers.
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION)

        // Indicates the state of Wi-Fi P2P connectivity has changed.
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION)

        // Indicates this device's details have changed.
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION)

        manager = getSystemService(Context.WIFI_P2P_SERVICE) as WifiP2pManager
        channel = manager.initialize(this, mainLooper, null)

        // interface
        serveripEditTxt.setText(m_serverIp)
        wifiSOOportEditTxt.setText(m_wifiSOOPort.toString())

        // Connexion button
        wifiConnectBtn.setOnClickListener {
            if(isWifiEnabled) {
                m_myComm.disconnect() // close Bluetooth
                m_myWifiComm.serverIp = m_serverIp
                m_myWifiComm.wifiSOOPort = m_wifiSOOPort
                m_myWifiComm.ConnectToWifiDevice().execute()

                disableEnableButtons(true)
            }
            else
                Toast.makeText(this@SOONetWifi, "Activez le wi-fi puis réessayer",   Toast.LENGTH_LONG).show()
        }

        disableEnableButtons(m_myWifiComm.m_isConnected)

        serveripEditTxt.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {}
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) { }
            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
                if(s.toString().matches(REGEX_IPADDRES)) {
                    m_serverIp = s.toString()
                    Toast.makeText(this@SOONetWifi, "Adresse IP mise à jour",   Toast.LENGTH_LONG).show()
                }
                else
                    Toast.makeText(this@SOONetWifi, "Adresse IP invalide",   Toast.LENGTH_LONG).show()
            }
        })
        wifiSOOportEditTxt.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {}
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) { }
            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
                m_wifiSOOPort = s.toString().toInt()
                Toast.makeText(this@SOONetWifi, "Port wifi SOO mis à jour",   Toast.LENGTH_LONG).show()
            }
        })
    }

    /** register the BroadcastReceiver with the intent values to be matched  */
    public override fun onResume() {
        super.onResume()
        //receiver = WiFiDirectBroadcastReceiver(manager, channel, this)
        registerReceiver(receiver, intentFilter)
    }

    public override fun onPause() {
        super.onPause()
        unregisterReceiver(receiver)
    }

    /*
    override fun onBackPressed() {
        super.onBackPressed()
        // back button goes home
        val intent = Intent(this, MainActivity :: class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
    }*/


    // Create a BroadcastReceiver for ACTION_FOUND.
    private val intentFilter = IntentFilter()
    private var receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            when(intent.action) {
                WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION -> {
                    // Determine if Wifi P2P mode is enabled or not, alert
                    // the Activity.
                    val state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1)
                    var isWifiP2pEnabled = state == WifiP2pManager.WIFI_P2P_STATE_ENABLED
                    isWifiEnabled = isWifiP2pEnabled
                    Log.i(TAG, "Wi-fi turned $isWifiP2pEnabled")
                }
                /* WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION -> {

                    // The peer list has changed

                }
                WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION -> {

                    // Connection state changed

                }
                WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION -> {
                    /*
                    (supportFragmentManager.findFragmentById(R.id.frag_list) as DeviceListFragment)
                        .apply {
                            updateThisDevice(
                                intent.getParcelableExtra(
                                    WifiP2pManager.EXTRA_WIFI_P2P_DEVICE) as WifiP2pDevice
                            )
                        }*/
                }*/
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        // Check which request we're responding to
        if (requestCode == REQUEST_ENABLE_WIFI) {
            // Make sure the request was successful
            /*if (resultCode == RESULT_OK) {
                Toast.makeText(this, "Wi-fi enabled", Toast.LENGTH_LONG).show()
                finish()
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }*/
            if (resultCode ==  RESULT_CANCELED) {
                Toast.makeText(this, "Wi-fi not enabled", Toast.LENGTH_LONG).show()
                val intent = Intent(this, MainActivity :: class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
        }

    }

    fun disableEnableButtons(isOn : Boolean){
        runOnUiThread{
            control_wifiaskagency.visibility = View.INVISIBLE
            control_wifiaskagency.isEnabled = isOn
            wifiDisconnectBtn.visibility = View.INVISIBLE
            wifiDisconnectBtn.isEnabled = isOn

            if(isOn) {
                // Disconnexion button
                wifiDisconnectBtn.visibility = View.VISIBLE
                control_wifiaskagency.visibility = View.VISIBLE

                control_wifiaskagency.setOnClickListener { m_myWifiComm.sendCommand(DATA_SOO_ASK) }
                wifiDisconnectBtn.setOnClickListener {
                    m_myWifiComm.disconnect()


                    disableEnableButtons(false)
                }
            }
        }
    }

    companion object {
        private lateinit var channel: WifiP2pManager.Channel
        private lateinit var manager: WifiP2pManager

        var m_serverIp: String = "192.168.1.100" // "192.168.1.100"
        var m_wifiSOOPort: Int = 5008 // 5008

        val REGEX_IPADDRES = "\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}".toRegex()

        private var isWifiEnabled = false
    }
}



