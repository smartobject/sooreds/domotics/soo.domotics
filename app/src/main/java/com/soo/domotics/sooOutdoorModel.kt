package com.soo.domotics

import kotlin.properties.Delegates
import kotlin.reflect.KProperty

class SOOOutdoorModel(
    var id: Int,
    //val agencyUID : ByteArray,
    name: String = "",
    active: Boolean = true,
    southSun: kotlin.Int = 0,
    westSun: kotlin.Int = 0,
    eastSun: kotlin.Int = 0,
    light: kotlin.Int = 0,
    temperature: kotlin.Int = 0,
    wind: kotlin.Int = 0,
    twilight: kotlin.Int = 0,
    rain: kotlin.Int = 0,
    rainIntensity: kotlin.Int = 0
    //,agencyUID : ByteArray
    ) : PropertyChangeAware() {

        private val observer = { property: KProperty<*>,
                                 oldValue: Any,
                                 newValue: Any ->
            propertyChangeSupport.firePropertyChange(property.name, oldValue, newValue)
        }

        // define which property need a ChangeListener
        var name: String by Delegates.observable(name, observer)
        var active: Boolean by Delegates.observable(active, observer)
        var southSun: kotlin.Int by Delegates.observable(southSun, observer)
        var westSun: kotlin.Int by Delegates.observable(westSun, observer)
        var eastSun: kotlin.Int by Delegates.observable(eastSun, observer)
        var light: kotlin.Int by Delegates.observable(light, observer)
        var temperature: kotlin.Int by Delegates.observable(temperature, observer)
        var wind: kotlin.Int by Delegates.observable(wind, observer)
        var twilight: kotlin.Int by Delegates.observable(twilight, observer)
        var rain: kotlin.Int by Delegates.observable(rain, observer)
        var rainIntensity: kotlin.Int by Delegates.observable(rainIntensity, observer)
        //var agencyUID: ByteArray by Delegates.observable(agencyUID, observer)

    override fun toString(): String {
        return name
    }

    companion object {
        fun updateModel(dst: SOOOutdoorModel, src: SOOOutdoorModel): SOOOutdoorModel = dst.apply {
            name = src.name
            active = src.active
            southSun = src.southSun
            westSun = src.westSun
            eastSun = src.eastSun
            light = src.light
            temperature = src.temperature
            wind = src.wind
            twilight = src.twilight
            rain = src.rain
            rainIntensity = src.rainIntensity
            id = src.id
        }
    }
}