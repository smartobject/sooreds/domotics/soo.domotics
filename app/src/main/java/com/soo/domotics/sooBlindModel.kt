package com.soo.domotics

import kotlin.properties.Delegates
import kotlin.reflect.KProperty

class SOOBlindModel(val id:Int,
                    //val agencyUID : ByteArray,
                    name:String,
                    blindphysposition:Int,
                    blindnextposition:Int = blindphysposition,
                    state:Int = 0,
                    active:Boolean = true,
                    calibrating:Boolean = false,
                    strongLight:Int = 0,
                    lowLight:Int = 0,
                    strongWind:Int = 0,
                    calibrationDone:Boolean = false
                    //, customState:Int = 0
                    //,agencyUID : ByteArray
) : PropertyChangeAware() {

    private val observer = {
            property: KProperty<*>,
            oldValue: Any,
            newValue: Any -> propertyChangeSupport.firePropertyChange(property.name, oldValue, newValue)
    }

    // define which property need a ChangeListener
    var blindphysposition:Int by Delegates.observable(blindphysposition, observer)
    var blindnextposition:Int by Delegates.observable(blindnextposition, observer)
    var name:String by Delegates.observable(name, observer)
    var strongLight: Int by Delegates.observable(strongLight, observer)
    var lowLight: Int by Delegates.observable(lowLight, observer)
    var strongWind: Int by Delegates.observable(strongWind, observer)
    var state: Int by Delegates.observable(state, observer)
    var active: Boolean by Delegates.observable(active, observer)
    var calibrating: Boolean by Delegates.observable(calibrating, observer) //29
    var calibrationDone: Boolean by Delegates.observable(calibrationDone, observer) //29

    //var agencyUID: ByteArray by Delegates.observable(agencyUID, observer)
    //var customState: Int by Delegates.observable(customState, observer)

    fun getStateSup(): Int { // 0 = Ready 1 = en montée 2 = en descente 3 = Processing 4 = en cours de calibratoin 5 = à calibrer
        var customState: Int
        customState = this.state
        if (this.calibrating) customState = 4
        if (!this.calibrationDone) customState = 5
        return customState
    }
}
