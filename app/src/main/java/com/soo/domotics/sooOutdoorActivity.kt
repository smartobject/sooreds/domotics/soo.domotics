package com.soo.domotics

//import android.app.ProgressDialog
import android.content.Context
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import java.nio.ByteBuffer
import java.nio.ByteOrder

import kotlinx.android.synthetic.main.activity_soo_outdoor.*
import java.beans.PropertyChangeListener
import android.util.Log
import android.view.View
import android.widget.*
import java.io.IOException


class SOOOutdoorActivity : AppCompatActivity() {
    private var TAG = "SOO.outdoor"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_soo_outdoor)
        supportActionBar?.title = "SOO.outdoor"
        //supportActionBar?.hide()

        var stringToToast = "Chargement des données SOO.outdoor..."
        when (CONNEXIONSTATE) {
            0 -> {
                //m_SOOOutdoor = SOOOutdoorModel(-1)
                sooOutdoorList.clear()
                stringToToast =
                    getString(R.string.msg_connectToASOO) //Log.i(TAG, "SOO.Blind wants to connect but is not connected")
            }
            1 -> m_myComm.sendCommand(DATA_SOO_OUTDOOR)
            2 -> m_myWifiComm.sendCommand(DATA_SOO_OUTDOOR)
        }
        Toast.makeText(this, stringToToast, Toast.LENGTH_SHORT).show()

        fun updateValues(t_SOOOutdoor: SOOOutdoorModel) {
            Log.i(TAG, "Updating Outdoor UI data")
            this.runOnUiThread {
                //if (t_SOOOutdoor.id != -1) {
                    SunpositionTxtv?.text = "(O: " + t_SOOOutdoor.westSun + "klx, S: " + t_SOOOutdoor.southSun +
                            "klx, E: " +
                            t_SOOOutdoor.eastSun + "klx)"
                    ambientluxTxtv?.text = "" + t_SOOOutdoor.light + "lx"
                    val realTemperature: Float = t_SOOOutdoor.temperature.toFloat() / 10
                    temperatureTxtv?.text = "" + realTemperature + "°C"
                    val realWind: Float = t_SOOOutdoor.wind.toFloat() / 10
                    WindspeedTxtv?.text = "" + realWind + "m/s"

                    if (t_SOOOutdoor.twilight == 1) crepdetectionTxtv?.text = "oui"
                    else crepdetectionTxtv?.text = "non"

                    if (t_SOOOutdoor.rain == 1) rainTxtv?.text = "oui"
                    else rainTxtv?.text = "non"

                    when (t_SOOOutdoor.rainIntensity) {
                        0 -> rainlevelTxtv?.text = "pas de pluie"
                        1 -> rainlevelTxtv?.text = "pluie faible"
                        2 -> rainlevelTxtv?.text = "pluie moyenne"
                        3 -> rainlevelTxtv?.text = "pluie forte"
                        else -> rainlevelTxtv?.text = "?"
                    }
                //}
            }
        }

        class Notifying : AsyncTask<Void, Void, String>() {
            override fun doInBackground(vararg p0: Void?): String? {
                val notifyAdapterThread = Thread {
                    SOOOutdoorActivity.notifyAdapterThreadStarted = true
                    while (true) {
                        try {

                            if (SOOOutdoorActivity.hasToNotifyDataSet) {
                                notifNow()
                            }
                            SOOOutdoorActivity.hasToNotifyDataSet = false

                            if(SOOOutdoorActivity.hasCurrentChanged)
                                updateValues(currentSelectedSOOOutdoor)
                            SOOOutdoorActivity.hasCurrentChanged = false

                            /*
                            if (updatedDataSet) {
                                Log.i(TAG,"updatedDataSet is $updatedDataSet hasToUpdateRightMenuValues is $hasToUpdateRightMenuValues rightMenFresh is $rightMenFresh")
                                updatedDataSet = false
                                notifNow()
                                if(hasToUpdateRightMenuValues)
                                    updateRightMenuValues(rightMenFresh)
                            }*/
                        } catch (e: IOException) {
                            e.printStackTrace()
                            break
                        }
                        Thread.sleep(200)
                    }
                    SOOBlindsActivity.notifyAdapterThreadStarted = false
                }
                if (notifyAdapterThread.state == Thread.State.NEW)
                    notifyAdapterThread.start()
                return null
            }

            private fun notifNow() {
                runOnUiThread {
                    SOOOutdoorActivity.arrayAdapterOutdoor.notifyDataSetChanged()
                }
            }
        }
        if (!SOOOutdoorActivity.notifyAdapterThreadStarted)
            Notifying().execute()

        if (sooOutdoorList.size == 0) {
            sooOutdoorList.add(0, noneSelected)
            currentSelectedSOOOutdoor = noneSelected
        }

        arrayAdapterOutdoor = SOOOutdoorArrayAdapter(this, android.R.layout.simple_spinner_item, sooOutdoorList)
        arrayAdapterOutdoor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        spinner = spinnerChoice

        spinner.adapter = arrayAdapterOutdoor

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                //SOOOutdoorModel.updateModel(currentSelectedSOOOutdoor, sooOutdoorList[position])
                currentSelectedSOOOutdoor = sooOutdoorList[position]
                updateValues(currentSelectedSOOOutdoor)
                //spinner.setSelection(position, true)

            }

        }

        currentSelectedSOOOutdoor.addPropertyChangeListener(
            PropertyChangeListener { event ->
                println("Property [${event.propertyName}] changed " + "from [${event.oldValue}] to [${event.newValue}]")
                updateValues(currentSelectedSOOOutdoor)
            })

        updateValues(currentSelectedSOOOutdoor)

        activityStarted = true
    }

    inner class SOOOutdoorArrayAdapter(var mCtx: Context, var resource: Int, var items: List<SOOOutdoorModel>) :
        ArrayAdapter<SOOOutdoorModel>(mCtx, resource, items) {
        override fun getCount(): Int {
            return items.size
        }

        // get item id position in the listview
        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

    }


    override fun onPause() {
        super.onPause()
        when (CONNEXIONSTATE) { // Make agency stop sending Outdoor by sending neutral data
            1 -> m_myComm.sendCommand(DATA_SOO_NEUTRAL)
            2 -> m_myWifiComm.sendCommand(DATA_SOO_NEUTRAL)
        }
    }

    override fun onResume() {
        super.onResume()

        var stringToToast = "Chargement des données SOO.outdoor..."
        when (CONNEXIONSTATE) {
            0 -> {
                //m_SOOOutdoor = SOOOutdoorModel(-1)
                sooOutdoorList.clear()
                stringToToast =
                    getString(R.string.msg_connectToASOO) //Log.i(TAG, "SOO.Blind wants to connect but is not connected")
            }
            1 -> m_myComm.sendCommand(DATA_SOO_OUTDOOR)
            2 -> m_myWifiComm.sendCommand(DATA_SOO_OUTDOOR)
        }
        if (sooOutdoorList.size==0)
            sooOutdoorList.add(0, noneSelected)
        Toast.makeText(this, stringToToast, Toast.LENGTH_SHORT).show()
    }

    override fun onDestroy() {
        super.onDestroy()
        activityStarted = false
    }


    companion object {
        //lateinit var m_progress: ProgressDialog
        var activityStarted = false
        var m_SOOOutdoor: SOOOutdoorModel = SOOOutdoorModel(-1)

        ///////////////////////////////////////
        private var sooOutdoorList = mutableListOf<SOOOutdoorModel>()// GENERAL LIST OF SOO.OUTDOORS
        private var currentSelectedSOOOutdoor = SOOOutdoorModel(-1, "-")
        lateinit var arrayAdapterOutdoor: SOOOutdoorArrayAdapter// = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sooOutdoorNamesList)
        lateinit var spinner: Spinner
        var hasCurrentChanged = false
        var hasToNotifyDataSet = false
        var notifyAdapterThreadStarted = false
        var outdoorSelected = false
        val noneSelected = SOOOutdoorModel(-1, "-")
        //////////////////////////////////////

        var m_sooOutdoorData = m_myComm.genData(DATA_SOO_OUTDOOR)
        //lateinit var m_sooOutdoorData : ByteArray // initialized by CommManager


        fun recvSOOOutdoorData() {
            Log.i(TAG, "Rcvd SOO Outdoor data")
            //m_myComm.genData("ask_SOOOutdoor")
            /*
            BT Packet header:
            0: Packet type (0 = beacon, 1 = data)
            1-16: ME SPID
            BT packet payload:
            17: Type (do not care)
            18-21: South Sun in klx
            22-25: West Sun in klx
            26-29: East Sun
            30-33: Light in lx
            34-37: Temperature in 0.1°C
            28-41: Wind in 0.1m/s
            42-43: Twilight (1 or 0)
            44-45: Rain (1 or 0)
            46-49: Rain intensity (enum: 0 - 3)
            */

            //============================================================================


            // Add a sooblind in the list
            fun addSOOoutdoorToList(sooOutdoortoAdd: SOOOutdoorModel) {

                sooOutdoorList.add(sooOutdoortoAdd)

                hasToNotifyDataSet = true
                //arrayAdapterOutdoor.notifyDataSetChanged()
                //sooOutdoorNamesList.add(sooOutdoortoAdd.name)

                //sooblindsList.sortBy { it.id }
            }

            // remove a sooblind from the list
            fun removeSOOoutdoorFromList(tmpID: Int) {

                sooOutdoorList.remove(sooOutdoorList.first { it.id == tmpID })
                hasToNotifyDataSet = true
                //arrayAdapterOutdoor.notifyDataSetChanged()
            }

            //============================================================================

            // TODO: find a way to pass these infos though header
            var inputData = m_sooOutdoorData

            var SOOoutdoorsListSize = 6

            var descPtr = m_myComm.pktPtr + m_myComm.headerPtr
            var descLen = 32

            var ecostabPtr = descPtr + descLen * SOOoutdoorsListSize + 4
            var ecostabLen = 52

            for (i in 0..SOOoutdoorsListSize - 1) { // todo clean everything (uniformisation of integers in int, compare directly boolean (why isActive would be int?), ...
                val isActiveInt = Integer(
                    ByteBuffer.wrap(
                        inputData.copyOfRange(
                            ecostabPtr,
                            ecostabPtr + 4
                        )
                    ).order(ByteOrder.LITTLE_ENDIAN).getInt()
                )

                if (sooOutdoorList.any { it.id == i }) {
                    if (isActiveInt.compareTo(1) != 0) {
                        //removeSOOblindFromList(i) //sooblindsList.remove(sooblindsList.first{it.id == i})
                        removeSOOoutdoorFromList(i)
                        Log.d(TAG, "SOO.outdoor removed from the list")
                    } else {

                        sooOutdoorList.first { it.id == i }.southSun =
                            ByteBuffer.wrap(inputData.copyOfRange(descPtr, descPtr + 4)).order(ByteOrder.LITTLE_ENDIAN)
                                .int
                        sooOutdoorList.first { it.id == i }.westSun =
                            ByteBuffer.wrap(inputData.copyOfRange(descPtr + 4, descPtr + 8))
                                .order(ByteOrder.LITTLE_ENDIAN).int
                        sooOutdoorList.first { it.id == i }.eastSun =
                            ByteBuffer.wrap(inputData.copyOfRange(descPtr + 8, descPtr + 12))
                                .order(ByteOrder.LITTLE_ENDIAN).int
                        sooOutdoorList.first { it.id == i }.light =
                            ByteBuffer.wrap(inputData.copyOfRange(descPtr + 12, descPtr + 16))
                                .order(ByteOrder.LITTLE_ENDIAN).int
                        sooOutdoorList.first { it.id == i }.temperature =
                            ByteBuffer.wrap(inputData.copyOfRange(descPtr + 16, descPtr + 20))
                                .order(ByteOrder.LITTLE_ENDIAN).int
                        sooOutdoorList.first { it.id == i }.wind =
                            ByteBuffer.wrap(inputData.copyOfRange(descPtr + 20, descPtr + 24))
                                .order(ByteOrder.LITTLE_ENDIAN).int
                        sooOutdoorList.first { it.id == i }.twilight = inputData[descPtr + 24].toInt()
                        sooOutdoorList.first { it.id == i }.rain = inputData[descPtr + 25].toInt()
                        sooOutdoorList.first { it.id == i }.rainIntensity =
                            ByteBuffer.wrap(inputData.copyOfRange(descPtr + 28, descPtr + 32))
                                .order(ByteOrder.LITTLE_ENDIAN).short.toInt()


                        sooOutdoorList.first { it.id == i }.active = isActiveInt.compareTo(1) == 0
                        sooOutdoorList.first { it.id == i }.name =
                            String(inputData.copyOfRange(ecostabPtr + 20, ecostabPtr + 36))

                        if (sooOutdoorList.first { it.id == i }.id == currentSelectedSOOOutdoor.id) {
                            //SOOOutdoorModel.updateModel(currentSelectedSOOOutdoor, sooOutdoorList.first { it.id == i })
                            currentSelectedSOOOutdoor = sooOutdoorList.first { it.id == i }
                            hasCurrentChanged = true
                            //hasToUpdateRightMenuValues = true
                            //SOOBlindsActivity.hasToSetSOODetails = true
                            Log.d(TAG, "SOO.outdoor selected received. LUX: ${currentSelectedSOOOutdoor.light}")
                        }
                        Log.d(TAG, "List size ${sooOutdoorList.size}")

                        //Log.i(TAG, "Descr age = "+ByteBuffer.wrap(inputData.copyOfRange(ecostabPtr + 4, ecostabPtr + 8)).order(ByteOrder.LITTLE_ENDIAN).getInt())
                        //Log.i(TAG, inputData.copyOfRange(ecostabPtr, ecostabPtr + 8).joinToString(" ") { String.format("%02X", (it.toInt() and 0xff))})
                    }
                } else if (isActiveInt.compareTo(1) == 0) {
                    addSOOoutdoorToList(SOOOutdoorModel(i, ""))
                    Log.d(TAG, "SOO.outdoor added to the list")
                }

                descPtr += descLen
                ecostabPtr += ecostabLen
            }


            //Log.i("data", ""+m_SOOOutdoor.southSun+", "+m_SOOOutdoor.westSun+", "+m_SOOOutdoor.eastSun+", "+m_SOOOutdoor.light+", "+m_SOOOutdoor.temperature)
        }
    }
}




