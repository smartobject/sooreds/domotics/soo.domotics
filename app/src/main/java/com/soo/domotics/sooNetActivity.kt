package com.soo.domotics

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_soo_net.*

import android.util.Log
import android.view.View

import android.content.*
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.*


private val REQUEST_ENABLE_BT = 1
val m_bluetoothAdapter: BluetoothAdapter? = BluetoothAdapter.getDefaultAdapter()
val m_myComm = CommManager()

class SOONetActivity : AppCompatActivity() {
    private var TAG = "SOO.NetBT"

    //private val REQUEST_ENABLE_BLUETOOTH = 1

    var PairedBTList = mutableListOf<String>();// LIST OF PAIRED BT

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_soo_net)
        supportActionBar?.title = "SOO.net: BLUETOOTH"
        //supportActionBar?.hide()

        btn_gotoWifi.setOnClickListener {
            val intent = Intent(this, SOONetWifi :: class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent) }

        // Init
        var BTListView = findViewById<ListView>(R.id.pairedBTListView)
        val BTListViewAdapter = BTArrayAdapter(this,R.layout.btlist_row,PairedBTList)
        BTListView.adapter = BTListViewAdapter


        // Bluetooth connexion
        if (m_bluetoothAdapter == null) {
            Toast.makeText(this, "Bluetooth is not supported on this device", Toast.LENGTH_LONG).show()
        }
        else{
            if (!m_bluetoothAdapter.isEnabled) {
                val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
            }

            //val pairedDevices: Set<BluetoothDevice> = m_bluetoothAdapter!!.bondedDevices
            pairedDevices = m_bluetoothAdapter!!.bondedDevices
            PairedBTList.clear()
            pairedDevices.forEach { device ->
                //val deviceName = device.name
                val deviceHardwareAddress = device.address // MAC address
                PairedBTList.add(deviceHardwareAddress)
                BTListViewAdapter.notifyDataSetChanged()
            }

            if(m_bluetoothAdapter.isEnabled) {
                Log.i(TAG, "Paired bt list size " + PairedBTList.size)
                if(PairedBTList.size < 1)
                    info_textview.text = "Appairez un SOO à votre appareil puis recommencez."
                else
                    info_textview.text = "Pour vos connecter à l'écosystème, cliquez sur un SOO présent dans la liste de vos appareils Bluetooth appairés :"
            }

            // Register for broadcasts when a device is discovered.
            //val intentFilter = IntentFilter(BluetoothDevice.ACTION_FOUND)
                intentFilter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
                intentFilter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
                intentFilter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
                intentFilter.addAction(BluetoothDevice.ACTION_FOUND);
                intentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
                intentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
            registerReceiver(receiver, intentFilter)

            // Click on a pairedBT
            pairedBTListView.onItemClickListener = AdapterView.OnItemClickListener {parent, view, position, id ->
                val device = pairedDevices.first{it.address == PairedBTList[position]}
                //deviceName_textview.text = "Connexion à "+device.name+"..."
                val address: String = device.address
                val name: String = device.name

                m_myWifiComm.disconnect() // close wifi
                m_myComm.m_address = address
                m_myComm.m_name = name
                m_myComm.ConnectToBTDevice(this).execute()
            }
        }

        disableEnableButtons(m_myComm.m_isConnected)
        if(m_myComm.m_isConnected)
            deviceName_textview.text = "Connecté à " + m_myComm.m_name
        else
            deviceName_textview.text = "Connectez un SOO via Bluetooth"
        //startedAct = true
    }

    override fun onStart(){
        super.onStart()
        disableEnableButtons(m_myComm.m_isConnected)
    }

    /** register the BroadcastReceiver with the intent values to be matched  */
    public override fun onResume() {
        super.onResume()
        registerReceiver(receiver, intentFilter) //receiver = WiFiDirectBroadcastReceiver(manager, channel, this)
    }

    public override fun onPause() {
        super.onPause()
        unregisterReceiver(receiver)
    }

    fun disableEnableButtons(isOn : Boolean){
        runOnUiThread{
            control_askagency.visibility = View.INVISIBLE
            control_askagency.isEnabled = isOn
            control_disconnect.visibility = View.INVISIBLE
            control_disconnect.isEnabled = isOn

            if(isOn) {
                // Disconnexion button
                control_disconnect.visibility = View.VISIBLE
                control_askagency.visibility = View.VISIBLE

                //deviceName_textview.text = m_myComm.m_name
                control_askagency.setOnClickListener { m_myComm.sendCommand(DATA_SOO_ASK) }
                control_disconnect.setOnClickListener {
                    deviceName_textview.text = "Déconnexion..."
                    m_myComm.disconnect()
                    //deviceName_textview.text = "Connect a SOO"
                }
            }
        }
    }


    // adapter class that returns the paired Bluetooth listview
    inner class BTArrayAdapter(var mCtx: Context, var resource: Int, var items: List<String>):
        ArrayAdapter<String>(mCtx, resource, items) {
        override fun getCount(): Int {
            return items.size
        }
        // get item id position in the listview
        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        // return the view with data
        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

            val layoutInflater: LayoutInflater = LayoutInflater.from(mCtx)
            val BTView: View = layoutInflater.inflate(resource, null)
            var BTName_textView: TextView = BTView.findViewById(R.id.BTName_textview)
            var BTMac_textView: TextView = BTView.findViewById(R.id.BTMac_textview)
            //BTMac_textView.text = items[position]
            BTName_textView.text = pairedDevices?.first{it.address == PairedBTList[position]}.name
            BTMac_textView.text = pairedDevices?.first{it.address == PairedBTList[position]}.address

            return BTView
        }
    }

    // Create a BroadcastReceiver for ACTION_FOUND.
    private val intentFilter = IntentFilter(BluetoothDevice.ACTION_FOUND)
    private val receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action: String = intent.action
            when (action) {
                BluetoothDevice.ACTION_FOUND -> {
                    //val device: BluetoothDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE)
                    //Device found
                    // Discovery has found a device. Get the BluetoothDevice
                    // object and its info from the Intent.
                    //val deviceName = device.name
                    //val deviceHardwareAddress = device.address // MAC address
                    // TODO later: ask agency what type of SOO THEN put it in the list (if SOO)
                }
                BluetoothDevice.ACTION_ACL_CONNECTED -> {
                    disableEnableButtons(true)
                    if(m_myComm.m_isConnected)
                        deviceName_textview.text = "Connecté à " + m_myComm.m_name
                    //Device is now connected
                }
                BluetoothAdapter.ACTION_DISCOVERY_FINISHED -> {
                    //m_bluetoothAdapter?.cancelDiscovery()
                    //Done searching
                }
                BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED -> {
                    //Device is about to disconnect

                }
                BluetoothDevice.ACTION_ACL_DISCONNECTED -> {
                    val device: BluetoothDevice =
                        intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE)
                    disableEnableButtons(false)
                    //Device has disconnected
                    deviceName_textview.text = device.name+" n'est plus connecté"
                    Log.i(TAG, device.name+" has been disconnected")
                }

                /*BluetoothAdapter.ACTION_STATE_CHANGED -> {
                    val state = intent.getIntExtra(
                        BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.ERROR
                    )
                    when (state) {
                        BluetoothAdapter.STATE_OFF -> btStateText.text = "Bluetooth off"
                        BluetoothAdapter.STATE_TURNING_OFF -> btStateText.text ="Turning Bluetooth off..."
                        BluetoothAdapter.STATE_ON -> btStateText.text ="Bluetooth on"
                        BluetoothAdapter.STATE_TURNING_ON -> btStateText.text ="Turning Bluetooth on..."
                    }

                }*/
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        // Check which request we're responding to
        if (requestCode == REQUEST_ENABLE_BT) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                Toast.makeText(this, "Bluetooth activé", Toast.LENGTH_LONG).show()
                finish()
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
            if (resultCode ==  RESULT_CANCELED) {
                Toast.makeText(this, "Bluetooth désactivé", Toast.LENGTH_LONG).show()
                val intent = Intent(this, SOONetWifi :: class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
        }
    }

    companion object {
        //var pairedDevices: Set<BluetoothDevice> = m_bluetoothAdapter!!.bondedDevices
        lateinit var pairedDevices: Set<BluetoothDevice>
    }
}