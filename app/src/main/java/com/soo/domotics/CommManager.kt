package com.soo.domotics

import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import android.os.AsyncTask
import android.util.Log
import java.io.IOException
import java.util.*
import java.io.BufferedInputStream
import java.io.BufferedOutputStream

import android.content.Context
import android.app.ProgressDialog
import java.beans.PropertyChangeEvent

val DATA_SOO_BLIND = "ask_SOO.Blind"
val DATA_SOO_OUTDOOR = "ask_SOO.Outdoor"
val DATA_SOO_NEUTRAL = "ask_neutral"
val DATA_SOO_ASK = "ask_whoareyou"
val DATA_SOO_DISCONNECTION = "say_disconnection"
val DATA_SOO_HI = "say_hi"

open class CommManager(var m_address: String = "", var m_name : String =""){
    private var TAG = "SOO.net - CommManager"

    private var m_myUUID: UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB")
    private var m_bluetoothSocket: BluetoothSocket? = null
    private lateinit var m_readStream : BufferedInputStream
    private lateinit var m_writeStream : BufferedOutputStream

    //lateinit var m_bluetoothAdapter: BluetoothAdapter
    var pktPtr: Int = 28
    var headerPtr = 18

    open var m_isConnected: Boolean = false
    open var disconnecting = false
    open var connecting = false

    //SOOOutdoorActivity.m_sooOutdoorData = m_myComm.genData("ask_SOOOutdoor")
    //SOOBlindsActivity.m_sooBlindData = m_myComm.genData("ask_SOOBlind")

    private val readThread = Thread{
        Log.i(TAG, "Readthread started ($m_isConnected)")
        var inputData = ByteArray(2048)

        while (true)
        {
            if(m_isConnected && !disconnecting) {
                if (m_bluetoothSocket!!.isConnected) {
                    try {
                        //Log.i(TAG, "before read "+m_bluetoothSocket!!.inputStream.available())
                        // Read from the InputStream
                        //val bfInputStream = BufferedInputStream(m_bluetoothSocket!!.inputStream) // bytes returned from read()
                        m_readStream.read(inputData)
                        recvData(inputData)


                        if(m_readStream.available() > 0) {
                            m_readStream.skip(m_readStream.available().toLong())
                            continue
                        }

                        //Log.i(TAG, "after read "+m_bluetoothSocket!!.inputStream.available())
                        //Log.i(TAG, "Reading ($m_isConnected)")
                    } catch (e: IOException) {
                        Log.i(TAG, "Disconnexion due to closed Bluetooth socket ($m_isConnected)")
                        disconnect()
                        //e.printStackTrace()
                    }
                } else {
                    Log.i(TAG, "Disconnexion - Bluetooth not connected.")
                    disconnect()
                }
            }
            //Thread.sleep(100)
        }
        Log.i(TAG, "Readthread ended ($m_isConnected)")
    }

    fun recvData(inputData: ByteArray) {
        dumpByteArray(inputData)

        // Giving an answer if receive a ping
        var tmpSOOdefault = genData(DATA_SOO_ASK)
        if (inputData[pktPtr + 17] == tmpSOOdefault[pktPtr + 17]){
            Log.i(TAG,"Received SOO ask")
            sendCommand(DATA_SOO_HI)
        }

        // SOO.Outdoor data
        var tmpSOOOutdoorData = genData(DATA_SOO_OUTDOOR)
        if (inputData[pktPtr + 14] == tmpSOOOutdoorData[pktPtr + 14] && inputData[pktPtr + 15] == tmpSOOOutdoorData[pktPtr + 15] && inputData[pktPtr + 16] == tmpSOOOutdoorData[pktPtr + 16]) {
            Log.i(TAG, "Got an outdoor ($m_isConnected)")
            // If a value has changed then send it to SOO.Outdoor
            if(!Arrays.equals(inputData, SOOOutdoorActivity.m_sooOutdoorData)) {
                //Log.i(TAG, "New Outdoor data change ($m_isConnected)")
                //SOOOutdoorActivity.m_sooOutdoorData = inputData
                SOOOutdoorActivity.m_sooOutdoorData = inputData.copyOf()
                if (SOOOutdoorActivity.activityStarted)
                    SOOOutdoorActivity.recvSOOOutdoorData()
            }
            //else Log.i(TAG, "not new")
        }
        // SOO.Blind data
        var tmpSOOBlindData = genData(DATA_SOO_BLIND)
        if (inputData[pktPtr + 14] == tmpSOOBlindData[pktPtr + 14] && inputData[pktPtr + 15] == tmpSOOBlindData[pktPtr + 15] && inputData[pktPtr + 16] == tmpSOOBlindData[pktPtr + 16]) {
            Log.i(TAG, "Got a blind ($m_isConnected)")
            // If value has changed then send it to SOO.Blind
            //if(!Arrays.equals(inputData, SOOBlindsActivity.m_sooBlindData)) {
                //Log.i(TAG, "New Blind data change ($m_isConnected)")
                //SOOBlindsActivity.m_sooBlindData = inputData.copyOf()
                SOOBlindsActivity.m_sooBlindData = inputData
                if (SOOBlindsActivity.activityStarted) {
                    SOOBlindsActivity.RecvSOOBlindData().execute()
                }
            //}
            //else Log.i(TAG, "not new")
        }
    }


    private fun dumpByteArray(data: ByteArray, isOutput:Boolean = false) {
        Log.i("data $isOutput", data.joinToString(" ") {
            String.format("%02X", (it.toInt() and 0xff))
        })
    }



    fun genData(commType : String = "default") : ByteArray{

        var outputData: ByteArray = ByteArray(2048)

        // data initialization
        /* BT Packet type: 0 = beacon */
        outputData[pktPtr + 0] = 0

        /* ME SPID */
        outputData[pktPtr + 1] = 0
        outputData[pktPtr + 2] = 0
        outputData[pktPtr + 3] = 0
        outputData[pktPtr + 4] = 0
        outputData[pktPtr + 5] = 0
        outputData[pktPtr + 6] = 0
        outputData[pktPtr + 7] = 0
        outputData[pktPtr + 8] = 0
        outputData[pktPtr + 9] = 0
        outputData[pktPtr + 10] = 0
        outputData[pktPtr + 11] = 0
        outputData[pktPtr + 12] = 0
        outputData[pktPtr + 13] = 0

        outputData[pktPtr + 14] = 0
        outputData[pktPtr + 15] = 0
        outputData[pktPtr + 16] = 0

        /* No payload */
        outputData[pktPtr + 17] = 0

        when (commType) {
            DATA_SOO_NEUTRAL -> {
                // Blinds
                outputData[pktPtr + 14] = Integer(0xff).toByte()
                outputData[pktPtr + 15] = Integer(0xff).toByte()
                outputData[pktPtr + 16] = Integer(0xff).toByte()
                outputData[pktPtr + 17] = 'C'.toByte()
            }
            DATA_SOO_BLIND -> {
                // Blinds
                outputData[pktPtr + 14] = Integer(0x0b).toByte()
                outputData[pktPtr + 15] = Integer(0x11).toByte()
                outputData[pktPtr + 16] = Integer(0x8d).toByte()
                outputData[pktPtr + 17] = 'C'.toByte()
            }
            DATA_SOO_OUTDOOR -> {
                // Outdoor
                outputData[pktPtr + 14] = Integer(0x61).toByte()
                outputData[pktPtr + 15] = Integer(0xd0).toByte()
                outputData[pktPtr + 16] = Integer(0x08).toByte()
                outputData[pktPtr + 17] = 'C'.toByte()
            }
            DATA_SOO_ASK -> {
                /* Connexion (who are you?) */
                outputData[pktPtr + 17] = '?'.toByte()
            }
            DATA_SOO_DISCONNECTION -> {
                /* Déconnexion */
                outputData[pktPtr + 17] = 'D'.toByte()
            }
            DATA_SOO_HI -> {
                /* Keep alive */
                outputData[pktPtr + 17] = '!'.toByte()
            }
        }

        return outputData
    }

    // sendSOOBlind(sooBlindtoAdd, event.propertyName, event.newValue.toString().toInt())
    fun sendSOOBlind(sooBlindtoSend: SOOBlindModel, event: PropertyChangeEvent) { // used by SOOBlindsActivity
        Log.i(TAG,"sendSOOBlind")
        var outputData = genData(DATA_SOO_BLIND)
        var len : Int
        var pktPtr = 28 // todo info à extraire, pas en dur...

        /* BT Packet type: 1 = data */
        outputData[pktPtr + 0] = 1

        var eventPropertyName = event.propertyName

        when(eventPropertyName){
            "blindnextposition" -> {
                val newValue = event.newValue.toString().toInt()
                val targetSooId = sooBlindtoSend.id
                Log.i(TAG, "target: " + targetSooId + ", newVal: "+newValue)

                // Si le SOO.blind est dans l'état "processing" la commande n'est pas envoyée
                //if (sooBlindtoSend.state != 0) {
                if (sooBlindtoSend.getStateSup() == 3) {
                    Log.i(TAG, "state="+sooBlindtoSend.getStateSup())
                    return
                }

                /* Command: IOCTL_SET_POS */
                outputData[pktPtr + 17] = 0x0c.toByte()
                outputData[pktPtr + 18] = 0x8d.toByte()
                outputData[pktPtr + 19] = 0x15.toByte()
                outputData[pktPtr + 20] = 0x4b.toByte()

                /*
                Argument
                0x0000xxyy
                xx: index of the Smart Object
                yy: target position
                 */
                outputData[pktPtr + 21] = newValue.toByte()
                outputData[pktPtr + 22] = targetSooId.toByte()
                outputData[pktPtr + 23] = 0
                outputData[pktPtr + 24] = 0

                len = 4 + 4
            }
            "lowLight" -> {
                var newValue = event.newValue.toString().toInt()
                val targetSooId = sooBlindtoSend.id
                Log.i("cmd", "target: " + targetSooId + ", newVal: " + newValue)

                if (newValue <= -1 || newValue >= 999) {
                    Log.i("cmd", "invalid lowLight="+newValue)
                    newValue = 0
                }

                /* Command: IOCTL_SET_LOW_LIGHT */
                outputData[pktPtr + 17] = 0x09.toByte()
                outputData[pktPtr + 18] = 0x8d.toByte()
                outputData[pktPtr + 19] = 0x15.toByte()
                outputData[pktPtr + 20] = 0x4b.toByte()

                /*
                Argument
                0x00xxyyyy
                xx: index of the Smart Object
                yyyy: target threshold
                 */
                val msb = (newValue and 0xff00) shr 8
                val lsb = newValue and 0xff
                outputData[pktPtr + 21] = lsb.toByte()
                outputData[pktPtr + 22] = msb.toByte()
                outputData[pktPtr + 23] = targetSooId.toByte()
                outputData[pktPtr + 24] = 0

                len = 4 + 4

                Log.i("data", outputData.joinToString(" ") {
                    String.format("%02X", (it.toInt() and 0xff))
                })
            }
            "strongLight" -> {
                var newValue = event.newValue.toString().toInt()
                val targetSooId = sooBlindtoSend.id
                Log.i("cmd", "target: " + targetSooId + ", newVal: "+newValue)

                if (newValue <= -1 || newValue >= 999) {
                    Log.i("cmd", "invalid strongLight="+newValue)
                    newValue = 0
                }

                /* Command: IOCTL_SET_STRONG_LIGHT */
                outputData[pktPtr + 17] = 0x0a.toByte()
                outputData[pktPtr + 18] = 0x8d.toByte()
                outputData[pktPtr + 19] = 0x15.toByte()
                outputData[pktPtr + 20] = 0x4b.toByte()

                /*
                Argument
                0x00xxyyyy
                xx: index of the Smart Object
                yyyy: target threshold
                 */
                val msb = (newValue and 0xff00) shr 8
                val lsb = newValue and 0xff
                outputData[pktPtr + 21] = lsb.toByte()
                outputData[pktPtr + 22] = msb.toByte()
                outputData[pktPtr + 23] = targetSooId.toByte()
                outputData[pktPtr + 24] = 0

                len = 4 + 4
            }
            "strongWind" -> {
                var newValue = event.newValue.toString().toInt()
                val targetSooId = sooBlindtoSend.id
                Log.i("cmd", "target: " + targetSooId + ", newVal: "+newValue)

                if (newValue <= -1 || newValue >= 300) {
                    Log.i("cmd", "invalid strongWind="+newValue)
                    newValue = 0
                }

                /* Command: IOCTL_SET_STRONG_WIND */
                outputData[pktPtr + 17] = 0x0b.toByte()
                outputData[pktPtr + 18] = 0x8d.toByte()
                outputData[pktPtr + 19] = 0x15.toByte()
                outputData[pktPtr + 20] = 0x4b.toByte()

                /*
                Argument
                0x00xxyyyy
                xx: index of the Smart Object
                yyyy: target threshold
                 */
                val msb = newValue and Integer(0xff00).toInt() shr 8
                val lsb = newValue and Integer(0xff).toInt()
                outputData[pktPtr + 21] = lsb.toByte()
                outputData[pktPtr + 22] = msb.toByte()
                outputData[pktPtr + 23] = targetSooId.toByte()
                outputData[pktPtr + 24] = 0

                len = 4 + 4
            }
            else -> return
        }


        /*if (eventPropertyName == "blindnextposition") {
            val newValue = event.newValue.toString().toInt()
            val targetSooId = sooBlindtoSend.id
            Log.i(TAG, "target: " + targetSooId + ", newVal: "+newValue)

            // Si le SOO.blind est dans l'état "processing" la commande n'est pas envoyée
            //if (sooBlindtoSend.state != 0) {
            if (sooBlindtoSend.getStateSup() == 3) {
                Log.i(TAG, "state="+sooBlindtoSend.getStateSup())
                return
            }

            /* Command: IOCTL_SET_POS */
            outputData[pktPtr + 17] = 0x0c.toByte()
            outputData[pktPtr + 18] = 0x8d.toByte()
            outputData[pktPtr + 19] = 0x15.toByte()
            outputData[pktPtr + 20] = 0x4b.toByte()

            /*
            Argument
            0x0000xxyy
            xx: index of the Smart Object
            yy: target position
             */
            outputData[pktPtr + 21] = newValue.toByte()
            outputData[pktPtr + 22] = targetSooId.toByte()
            outputData[pktPtr + 23] = 0
            outputData[pktPtr + 24] = 0

            len = 4 + 4
        } else if (eventPropertyName == "lowLight") {
            var newValue = event.newValue.toString().toInt()
            val targetSooId = sooBlindtoSend.id
            Log.i("cmd", "target: " + targetSooId + ", newVal: " + newValue)

            if (newValue <= -1 || newValue >= 999) {
                Log.i("cmd", "invalid lowLight="+newValue)
                newValue = 0
            }

            /* Command: IOCTL_SET_LOW_LIGHT */
            outputData[pktPtr + 17] = 0x09.toByte()
            outputData[pktPtr + 18] = 0x8d.toByte()
            outputData[pktPtr + 19] = 0x15.toByte()
            outputData[pktPtr + 20] = 0x4b.toByte()

            /*
            Argument
            0x00xxyyyy
            xx: index of the Smart Object
            yyyy: target threshold
             */
            val msb = (newValue and 0xff00) shr 8
            val lsb = newValue and 0xff
            outputData[pktPtr + 21] = lsb.toByte()
            outputData[pktPtr + 22] = msb.toByte()
            outputData[pktPtr + 23] = targetSooId.toByte()
            outputData[pktPtr + 24] = 0

            len = 4 + 4

            Log.i("data", outputData.joinToString(" ") {
                String.format("%02X", (it.toInt() and 0xff))
            })
        } else if (eventPropertyName == "strongLight") {
            var newValue = event.newValue.toString().toInt()
            val targetSooId = sooBlindtoSend.id
            Log.i("cmd", "target: " + targetSooId + ", newVal: "+newValue)

            if (newValue <= -1 || newValue >= 999) {
                Log.i("cmd", "invalid strongLight="+newValue)
                newValue = 0
            }

            /* Command: IOCTL_SET_STRONG_LIGHT */
            outputData[pktPtr + 17] = 0x0a.toByte()
            outputData[pktPtr + 18] = 0x8d.toByte()
            outputData[pktPtr + 19] = 0x15.toByte()
            outputData[pktPtr + 20] = 0x4b.toByte()

            /*
            Argument
            0x00xxyyyy
            xx: index of the Smart Object
            yyyy: target threshold
             */
            val msb = (newValue and 0xff00) shr 8
            val lsb = newValue and 0xff
            outputData[pktPtr + 21] = lsb.toByte()
            outputData[pktPtr + 22] = msb.toByte()
            outputData[pktPtr + 23] = targetSooId.toByte()
            outputData[pktPtr + 24] = 0

            len = 4 + 4
        } else if (eventPropertyName == "strongWind") {
            var newValue = event.newValue.toString().toInt()
            val targetSooId = sooBlindtoSend.id
            Log.i("cmd", "target: " + targetSooId + ", newVal: "+newValue)

            if (newValue <= -1 || newValue >= 300) {
                Log.i("cmd", "invalid strongWind="+newValue)
                newValue = 0
            }

            /* Command: IOCTL_SET_STRONG_WIND */
            outputData[pktPtr + 17] = 0x0b.toByte()
            outputData[pktPtr + 18] = 0x8d.toByte()
            outputData[pktPtr + 19] = 0x15.toByte()
            outputData[pktPtr + 20] = 0x4b.toByte()

            /*
            Argument
            0x00xxyyyy
            xx: index of the Smart Object
            yyyy: target threshold
             */
            val msb = newValue and Integer(0xff00).toInt() shr 8
            val lsb = newValue and Integer(0xff).toInt()
            outputData[pktPtr + 21] = lsb.toByte()
            outputData[pktPtr + 22] = msb.toByte()
            outputData[pktPtr + 23] = targetSooId.toByte()
            outputData[pktPtr + 24] = 0

            len = 4 + 4
        } else
            return*/

        dumpByteArray(outputData)
        sendData(outputData, len)

    }

    fun sendCommand(input: String) {
        var toSend = genData(input)
        sendData(toSend, 1)
    }

    open fun sendData(output: ByteArray, len: Int = 1) {
        if(m_isConnected) {
            //if (m_bluetoothSocket != null) {
            if (m_bluetoothSocket!!.isConnected) {
                try {
                    var tmpAddress = m_bluetoothSocket!!.remoteDevice.address
                    var tmpName = m_bluetoothSocket!!.remoteDevice.name
                    Log.i(TAG, "Sending data to $tmpName ($tmpAddress) ($m_isConnected)")
                    m_writeStream.write(output, 0, (18 + 28 + len))
                    m_writeStream.flush()
                } catch (e: IOException) {
                    Log.i(TAG, "Disconnexion - Data couldn't be sent to $m_name ($m_address) ($m_isConnected)")
                    disconnect()
                    e.printStackTrace()
                }
            }
        }
    }

    //inner class ConnectToBTDevice(c: Context) : AsyncTask<Void, Void, String>() {
    inner class ConnectToBTDevice(c : Context) : AsyncTask<Void, Void, String>() {
        private var connectSuccess: Boolean = true
        private val context = c
        lateinit var m_progress : ProgressDialog

        override fun onPreExecute() {
            super.onPreExecute()
            Log.i(TAG, "Trying to connect via BT...")
            m_progress = ProgressDialog.show(context, "Connecting...", "please wait")
            disconnect()
            connecting = true
        }

        override fun doInBackground(vararg p0: Void?): String? {
            try {
                //if (m_bluetoothSocket == null || !m_isConnected) {
                if (!m_isConnected) {
                    val device: BluetoothDevice = m_bluetoothAdapter!!.getRemoteDevice(m_address)
                    //m_bluetoothSocket = device.createInsecureRfcommSocketToServiceRecord(m_myUUID)
                    m_bluetoothSocket = device.createRfcommSocketToServiceRecord(m_myUUID)
                    //BluetoothAdapter.getDefaultAdapter().cancelDiscovery() // use it when using scanning
                    m_bluetoothSocket!!.connect()
                    m_readStream = BufferedInputStream(m_bluetoothSocket!!.inputStream) // bytes returned from read()
                    m_writeStream = BufferedOutputStream(m_bluetoothSocket!!.outputStream) // bytes returned from read()
                }
            } catch (e: IOException) {
                connectSuccess = false
                e.printStackTrace()
            }
            return null
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            if (!connectSuccess) {
                m_isConnected = false
                CONNEXIONSTATE = 0
                Log.i(TAG, "Couldn't connect ($m_isConnected)")
            } else {
                m_isConnected = true
                CONNEXIONSTATE = 1
                if (readThread.state == Thread.State.NEW)
                    readThread.start()
                sendCommand(DATA_SOO_ASK)
                Log.i(TAG, "Start reading from $m_name ($m_address) ($m_isConnected)")
            }
            m_progress.dismiss()
            connecting = false
        }
    }

    @Synchronized open fun disconnect() {
        Log.i(TAG,"Disconnecting BT $m_isConnected")
        if (m_bluetoothSocket != null && !disconnecting && !connecting) {
            try {
                disconnecting = true
                sendCommand(DATA_SOO_DISCONNECTION)
                m_bluetoothSocket!!.inputStream.close()
                m_bluetoothSocket!!.outputStream.close()
                try {
                    m_bluetoothSocket!!.close()
                } catch(e: IOException){
                    Log.e(TAG, "m_bluetoothSocket can't be closed $m_isConnected")
                    e.printStackTrace()
                }
                m_bluetoothSocket = null
                m_isConnected = false
                CONNEXIONSTATE = 0
            } catch (e: IOException) {
                e.printStackTrace()
                Log.e(TAG, "Couldn't close socket ($m_isConnected)")
            }
        }
        else if(m_bluetoothSocket == null || !m_bluetoothSocket!!.isConnected) {
            m_isConnected = false
            CONNEXIONSTATE = 0
        }

        disconnecting = false
        //finish()
    }
}