package com.soo.domotics

import android.os.AsyncTask
import android.util.Log
import java.io.IOException
import java.net.InetAddress
import java.net.Socket
import java.io.BufferedInputStream
import java.io.BufferedOutputStream

class WifiCommManager(var serverIp : String = "", var wifiSOOPort: Int = -1) : CommManager(){
    private var TAG = "SOO.net - Wifi CommManager"

    private var m_wifiSocket: Socket? = null
    private lateinit var m_readStream : BufferedInputStream
    private lateinit var m_writeStream : BufferedOutputStream

    override var m_isConnected: Boolean = false
    override var disconnecting = false

    val wifiReadThread = Thread{

        Log.i(TAG, "wifiReadThread started ($m_isConnected)")
        var inputData = ByteArray(2048)
        while (true) {
            if (m_isConnected) {
                if(m_wifiSocket!!.isConnected && !m_wifiSocket!!.isClosed) {
                    try {
                        // Read from the InputStream
                        m_readStream.read(inputData) //var len: Int = m_compServer.readStream!!.read(inputData)
                        //var len: Int = m_readStream!!.read(inputData)
                        recvData(inputData)
                        Log.i(TAG, "Reading ($m_isConnected)")

                        if(m_readStream.available() > 0) {
                            m_readStream.skip(m_readStream.available().toLong())
                            continue
                        }
                    } catch (e: IOException) {
                        Log.i(TAG, "Disconnection ($m_isConnected)")
                        disconnect()
                        e.printStackTrace()

                        m_isConnected = false
                        disconnecting = false
                    }
                }
                else disconnect()
            }
            Thread.sleep(100)
        }
        Log.i(TAG, "wifiReadThread ended ($m_isConnected)")
    }


    override fun sendData(output: ByteArray, len: Int) {
        Log.i(TAG,"")
        if(m_isConnected) {
            //if (m_bluetoothSocket != null) {
            if (m_wifiSocket!!.isConnected) {
                try {
                    m_writeStream.write(output, 0, (18 + 28 + len))
                    m_writeStream.flush()
                    //m_bluetoothSocket!!.outputStream.flush()
                    Log.i(TAG, "Data sent via wi-fi ($m_isConnected)")
                } catch (e: IOException) {
                    Log.e(TAG, "Disconnexion - Data couldn't be sent via wi-fi ($m_isConnected)")
                    disconnect()
                    e.printStackTrace()
                }
            }
        }
    }

    inner class ConnectToWifiDevice(): AsyncTask<Void, Void, String>() {
        private var connectSuccess: Boolean = true
        override fun onPreExecute() {
            super.onPreExecute()
            Log.i(TAG, "Trying to connect via Wi-Fi...")
            //m_progress = ProgressDialog.show(this@SOONetWifi, "Connecting...", "please wait")
        }
        override fun doInBackground(vararg p0: Void?): String? {
            try {
                //SOONetWifi.m_wifiServer = SOONetWifi.wifiServer(SOONetWifi.m_serverIp, SOONetWifi.m_toSOOPort, SOONetWifi.m_fromSOOPort)
                var serverAddr: InetAddress = InetAddress.getByName(serverIp)
                m_wifiSocket =  Socket(serverAddr, wifiSOOPort)
                m_readStream = BufferedInputStream(m_wifiSocket!!.inputStream)
                m_writeStream = BufferedOutputStream(m_wifiSocket!!.outputStream)
                //var readStream = BufferedInputStream(readSocket.inputStream)
                //var writeStream = BufferedOutputStream(writeSocket.outputStream)

            } catch (e: IOException) {
                connectSuccess = false
                Log.e(TAG, "Couldn't connect via Wi-Fi")
                e.printStackTrace()
                return null
            }
            return null
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            if (!connectSuccess) {
                m_isConnected = false
                CONNEXIONSTATE = 0
                Log.i(TAG, "Couldn't connect ($m_isConnected)")
            } else {
                m_isConnected = true
                CONNEXIONSTATE = 2
                if (wifiReadThread.state == Thread.State.NEW)
                    wifiReadThread.start()
                sendCommand(DATA_SOO_ASK)
                Log.i(TAG, "Start reading from wifi device... ($m_isConnected)") //todo put name & ip address
            }
            //m_progress.dismiss()
        }
    }

    override fun disconnect(){
        if (m_wifiSocket != null && !disconnecting) {
            if(!m_wifiSocket!!.isClosed) {
                try {
                    disconnecting = true
                    sendCommand(DATA_SOO_DISCONNECTION)

                    // TODO do more tests and debug it in case
                    try {
                        m_wifiSocket!!.inputStream.close()
                    } catch (e: IOException) {
                        //Log.e(TAG,"can't close wifi inputStream")
                        e.printStackTrace()}
                    try {
                        m_wifiSocket!!.outputStream.close()
                    } catch (e: IOException) {
                        //Log.e(TAG,"can't close wifi inputStream")
                        e.printStackTrace()}

                    try {
                        m_wifiSocket!!.close()
                    } catch (e: IOException) {
                        //Log.e(TAG,"can't close wifi socket")
                        e.printStackTrace()}

                    try {
                        m_wifiSocket = null
                    } catch (e: IOException) {
                        //Log.e(TAG,"can't make null wifi socket")
                        e.printStackTrace()}

                    try {
                        m_isConnected = false
                    } catch (e: IOException) {
                        //Log.e(TAG,"can't make false isConnected")
                        e.printStackTrace()}

                    CONNEXIONSTATE = 0

                    /*
                    m_wifiSocket!!.inputStream.close()
                    m_wifiSocket!!.outputStream.close()
                    m_wifiSocket!!.close()
                    m_wifiSocket = null
                    m_isConnected = false
                    */
                } catch (e: IOException) {
                    //e.printStackTrace()
                    Log.e(TAG, "Couldn't close wifi socket ($m_isConnected)")
                }
            }
        }
        disconnecting = false
    }
}
