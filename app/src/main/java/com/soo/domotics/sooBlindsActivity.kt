package com.soo.domotics

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import android.content.Context
import android.os.AsyncTask
import android.util.Log

import android.view.View
import android.widget.*

import android.view.LayoutInflater
import android.widget.TextView

import android.view.ViewGroup
import android.widget.ListView

import android.view.View.OnFocusChangeListener
import android.view.inputmethod.EditorInfo
import kotlinx.android.synthetic.main.activity_soo_blinds.*

import java.beans.PropertyChangeListener
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.io.IOException

private val LOWLIGHT_MAXVAL = 999
private val STRONGLIGHT_MAXVAL = 999
private val STRONGWIND_MAXVAL = 30
class SOOBlindsActivity : AppCompatActivity() {
    private var TAG = "SOO.Blind"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_soo_blinds)
        supportActionBar?.title = "SOO.blind"
        //supportActionBar?.hide()

        SOOblindsListAdapter = SOOblindArrayAdapter(this,R.layout.sooblindlist_row,sooBlindsList)

        var stringToToast = "Chargement des SOO.blinds..."
        when(CONNEXIONSTATE){
            0 -> {
                sooBlindsList.clear()
                //SOOblindsListAdapter.notifyDataSetChanged()
                stringToToast = getString(R.string.msg_connectToASOO) //Log.i(TAG, "SOO.Blind wants to connect but is not connected")
            }
            1 -> m_myComm.sendCommand(DATA_SOO_BLIND)
            2 -> m_myWifiComm.sendCommand(DATA_SOO_BLIND)
        }
        Toast.makeText(this, stringToToast, Toast.LENGTH_SHORT).show()

        fun setSOODetails(tmpSOOBlind: SOOBlindModel = currentSelectedSOOBlind){
            runOnUiThread {
                if(tmpSOOBlind.id != -1) {
                    blindstateBtn.text = resources.getStringArray(R.array.sooBlindState_array)[tmpSOOBlind.getStateSup()]
                    blindsliderOldValTxt?.text = tmpSOOBlind.blindphysposition.toString()
                    blindsliderOldValTxt?.visibility = View.INVISIBLE
                    objNameTxt.text = tmpSOOBlind.name
                }

                blindsliderValTxt?.text = blindSeekbar.progress.toString() + "%"
                blindSeekbar.progress = tmpSOOBlind.blindnextposition

                if (syn_sooOutdoor_on) {
                    synOutdoorLyt.visibility = View.VISIBLE
                    //switch_stronglight.isChecked = tmpSOOBlind.strongLight > 0
                    /*switch_lowlight.isChecked = tmpSOOBlind.lowLight > 0
                    switch_strongwind.isChecked = tmpSOOBlind.strongWind > 0

                    //if (!obj_synoutdoor_stronglight.isFocused)
                    //    obj_synoutdoor_stronglight.setText(tmpSOOBlind.strongLight.toString())
                    obj_synoutdoor_lowlight.setText(tmpSOOBlind.lowLight.toString())
                    obj_synoutdoor_strongwind.setText(tmpSOOBlind.strongWind.toString())

                    //obj_synoutdoor_stronglight.isEnabled = switch_stronglight.isChecked
                    obj_synoutdoor_strongwind.isEnabled = switch_strongwind.isChecked
                    obj_synoutdoor_lowlight.isEnabled = switch_lowlight.isChecked*/
                }

                disableEnableControls(tmpSOOBlind.id != -1 && (tmpSOOBlind.getStateSup() == 0 || tmpSOOBlind.getStateSup() == 5) && tmpSOOBlind.active, rightMenuLyt) // maybe use rightMenuLyt.visibility = View.INVISIBLE
            }
        }

        // Outdoor synergy setting
        synOutdoor_radioBtn.isChecked = syn_sooOutdoor_on //syn_sooOutdoor_on = synOutdoor_radioBtn.isChecked

        fun synSOOoutdoor(tmpIsChecked : Boolean){
            if(tmpIsChecked) {
                synOutdoorLyt.visibility = View.VISIBLE
                setSOODetails()
                //updateRightMenuValues()
            }
            else{
                synOutdoorLyt.visibility = View.INVISIBLE
            }
        }

        synSOOoutdoor(syn_sooOutdoor_on)

        synOutdoor_radioBtn.setOnCheckedChangeListener { buttonView, isChecked ->
            //Toast.makeText(this,"SOO.outdoor : "+isChecked.toString(),Toast.LENGTH_LONG).show()
            syn_sooOutdoor_on = isChecked
            synSOOoutdoor(syn_sooOutdoor_on)
            SOOblindsListAdapter.notifyDataSetChanged()
        }


        /* Lose focus when clicking OK button on keyboard */
        obj_synoutdoor_stronglight.setOnEditorActionListener { v, actionId, event ->
            if(actionId == EditorInfo.IME_ACTION_DONE){

                obj_synoutdoor_stronglight.clearFocus()
            }
            true
        }
        obj_synoutdoor_lowlight.setOnEditorActionListener { v, actionId, event ->
            if(actionId == EditorInfo.IME_ACTION_DONE){

                obj_synoutdoor_lowlight.clearFocus()
            }
            true
        }
        obj_synoutdoor_strongwind.setOnEditorActionListener { v, actionId, event ->
            if(actionId == EditorInfo.IME_ACTION_DONE){

                obj_synoutdoor_strongwind.clearFocus()
            }
            true
        }


        // Right view initialization
        setSOODetails(currentSelectedSOOBlind)
        /* blindstateBtn.text = resources.getStringArray(R.array.sooBlindState_array)[currentSelectedSOOBlind.getStateSup()]
        blindsliderValTxt?.text = "" + blindSeekbar.progress.toString + "%"
        blindsliderOldValTxt?.text = currentSelectedSOOBlind.blindphysposition.toString()
        blindsliderOldValTxt?.visibility = View.INVISIBLE;*/

        blindSeekbar?.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                blindsliderValTxt?.text = "" + blindSeekbar.progress + "%"
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
                blindsliderOldValTxt?.visibility = View.VISIBLE
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                blindsliderOldValTxt?.visibility = View.INVISIBLE;
                //blindsliderOldValTxt?.text = blindsliderValTxt?.text
                blindsliderOldValTxt?.text = currentSelectedSOOBlind.blindphysposition.toString()
                currentSelectedSOOBlind.blindnextposition = blindSeekbar.progress
                disableEnableControls(false, rightMenuLyt)
                Log.i(TAG, "disabled details due to onStopTrackingTouch")
            }
        })

        // Left view list Initialization
        var SOOblindsListView = findViewById<ListView>(R.id.sooblindsListView)
        SOOblindsListView.adapter = SOOblindsListAdapter


        fun updateSyn(tmpSOOBlind: SOOBlindModel) {
            if(tmpSOOBlind.id != -1) {
                blindstateBtn.text = resources.getStringArray(R.array.sooBlindState_array)[tmpSOOBlind.getStateSup()]
                blindsliderOldValTxt?.text = tmpSOOBlind.blindphysposition.toString()
                blindsliderOldValTxt?.visibility = View.INVISIBLE
                objNameTxt.text = tmpSOOBlind.name
            }

            blindsliderValTxt?.text = blindSeekbar.progress.toString() + "%"
            blindSeekbar.progress = tmpSOOBlind.blindphysposition

            runOnUiThread {
                obj_synoutdoor_stronglight.setText(tmpSOOBlind.strongLight.toString())
                obj_synoutdoor_lowlight.setText(tmpSOOBlind.lowLight.toString())
                obj_synoutdoor_strongwind.setText(tmpSOOBlind.strongWind.toString())

                switch_stronglight.isChecked = tmpSOOBlind.strongLight > 0
                switch_lowlight.isChecked = tmpSOOBlind.lowLight > 0
                switch_strongwind.isChecked = tmpSOOBlind.strongWind > 0
            }
        }


        // Click on a SOOblind // check SOOblindsListView.onItemClickListener
        SOOblindsListView.setOnItemClickListener{parent, view, position, id ->
            currentSelectedSOOBlind = sooBlindsList[position] // update currentselectedBlind
            //updateRightMenuValues(true, currentSelectedSOOBlind);
            updateSyn(currentSelectedSOOBlind)
            rightMenuLyt.visibility = View.VISIBLE
        }

        // Outdoor synergy initialization
        obj_synoutdoor_lowlight.setText(currentSelectedSOOBlind.lowLight.toString())
        obj_synoutdoor_stronglight.setText(currentSelectedSOOBlind.strongLight.toString())
        obj_synoutdoor_strongwind.setText(currentSelectedSOOBlind.strongWind.toString())
        switch_lowlight.isChecked = currentSelectedSOOBlind.lowLight > 0
        switch_stronglight.isChecked = currentSelectedSOOBlind.strongLight > 0
        switch_strongwind.isChecked = currentSelectedSOOBlind.strongWind > 0
        /*obj_synoutdoor_stronglight.isEnabled = switch_stronglight.isChecked
        obj_synoutdoor_strongwind.isEnabled = switch_strongwind.isChecked
        obj_synoutdoor_lowlight.isEnabled = switch_lowlight.isChecked*/
        obj_synoutdoor_stronglight.isEnabled = true
        obj_synoutdoor_strongwind.isEnabled = true
        obj_synoutdoor_lowlight.isEnabled = true


        // Outdoor synergy options
        /* Focus change listener on the thresholds EditText. Needed to update the thresholds.
         * It also does a check on the value typed by the user
         *
         */
        obj_synoutdoor_lowlight?.onFocusChangeListener = OnFocusChangeListener {view, element -> if (!view.hasFocus())
            if(obj_synoutdoor_lowlight.text.toString().isNotEmpty()) {
                var tmpVal = obj_synoutdoor_lowlight.text.toString().toInt()
                if(tmpVal > LOWLIGHT_MAXVAL) tmpVal = LOWLIGHT_MAXVAL
                if(tmpVal <= 0) tmpVal = 1

                if (switch_lowlight.isChecked) {
                    currentSelectedSOOBlind.lowLight = tmpVal
                    sooBlindsList.first { it.id == currentSelectedSOOBlind.id }.lowLight = tmpVal
                }
            }
            else
                obj_synoutdoor_lowlight.setText(currentSelectedSOOBlind.lowLight.toString())
        }
        obj_synoutdoor_stronglight?.onFocusChangeListener = OnFocusChangeListener {view, element -> if (!view.hasFocus())
            if(obj_synoutdoor_stronglight.text.toString().isNotEmpty()) {
                var tmpVal = obj_synoutdoor_stronglight.text.toString().toInt()
                if(tmpVal > STRONGLIGHT_MAXVAL) tmpVal = STRONGLIGHT_MAXVAL
                if(tmpVal <= 0) tmpVal = 1

                if (switch_stronglight.isChecked) {
                    currentSelectedSOOBlind.strongLight = tmpVal
                    sooBlindsList.first { it.id == currentSelectedSOOBlind.id }.strongLight = tmpVal
                }
            }
            else
                obj_synoutdoor_stronglight.setText(currentSelectedSOOBlind.strongLight.toString())
        }
        obj_synoutdoor_strongwind?.onFocusChangeListener = OnFocusChangeListener {view, element -> if (!view.hasFocus())
            if(obj_synoutdoor_strongwind.text.toString().isNotEmpty()) {
                var tmpVal = obj_synoutdoor_strongwind.text.toString().toInt() * 10
                if(tmpVal > STRONGWIND_MAXVAL) tmpVal = STRONGWIND_MAXVAL
                if(tmpVal <= 0) tmpVal = 1

                if (switch_strongwind.isChecked) {
                    currentSelectedSOOBlind.strongWind = tmpVal
                    sooBlindsList.first { it.id == currentSelectedSOOBlind.id }.strongWind = tmpVal

                }
            }
            else
                obj_synoutdoor_strongwind.setText(currentSelectedSOOBlind.lowLight.toString())
        }


        /*
         * Change listener on the switches, required to change the current blind synergy thresholds
         */
        switch_stronglight?.setOnCheckedChangeListener { buttonView, isChecked ->
            var value = 0
            if(isChecked)
                value = obj_synoutdoor_stronglight.text.toString().toInt()

            currentSelectedSOOBlind.strongLight = obj_synoutdoor_stronglight.text.toString().toInt()
            sooBlindsList.first { it.id == currentSelectedSOOBlind.id }.strongLight = value
        }
        switch_lowlight?.setOnCheckedChangeListener { buttonView, isChecked ->
            var value = 0
            if(isChecked)
                value = obj_synoutdoor_lowlight.text.toString().toInt()

            currentSelectedSOOBlind.lowLight = value
            sooBlindsList.first { it.id == currentSelectedSOOBlind.id }.lowLight = value
        }
        switch_strongwind?.setOnCheckedChangeListener { buttonView, isChecked ->
            var value = 0
            if(isChecked)
                value = obj_synoutdoor_strongwind.text.toString().toInt()

            currentSelectedSOOBlind.strongWind = value
            sooBlindsList.first { it.id == currentSelectedSOOBlind.id }.strongWind = value
        }


        disableEnableControls(false, rightMenuLyt)
        setSOODetails()

        activityStarted = true

        class Notifying: AsyncTask<Void, Void, String>() {
            override fun doInBackground(vararg p0: Void?): String? {
                val notifyAdapterThread = Thread {
                    notifyAdapterThreadStarted = true
                    while (true) {
                        try {

                            if(hasToNotifyDataSet)
                                notifNow()
                            hasToNotifyDataSet = false

                            if(hasToSetSOODetails)
                                setSOODetails()
                            hasToSetSOODetails = false

                            /*
                            if (updatedDataSet) {
                                Log.i(TAG,"updatedDataSet is $updatedDataSet hasToUpdateRightMenuValues is $hasToUpdateRightMenuValues rightMenFresh is $rightMenFresh")
                                updatedDataSet = false
                                notifNow()
                                if(hasToUpdateRightMenuValues)
                                    updateRightMenuValues(rightMenFresh)
                            }*/
                        } catch (e: IOException) {
                            e.printStackTrace()
                            break
                        }
                        Thread.sleep(200)
                    }
                    notifyAdapterThreadStarted = false
                }
                if (notifyAdapterThread.state == Thread.State.NEW)
                    notifyAdapterThread.start()
                return null
            }
            private fun notifNow() {
                runOnUiThread{
                    SOOblindsListAdapter.notifyDataSetChanged()
                    setSOODetails()
                    //updateRightMenuValues(true) // security // can be optimized
                }
            }
        }
        if(!notifyAdapterThreadStarted)
            Notifying().execute()
    }

    override fun onPause() {
        super.onPause()

        when(CONNEXIONSTATE){ // Make agency stop sending Blinds by sending neutral data
            1 -> m_myComm.sendCommand(DATA_SOO_NEUTRAL)
            2 -> m_myWifiComm.sendCommand(DATA_SOO_NEUTRAL)
        }
    }

    override fun onResume() {
        super.onResume()

        disableEnableControls(currentSelectedSOOBlind.id != -1, rightMenuLyt)

        var stringToToast = "Chargement des SOO.blinds..."
        when(CONNEXIONSTATE){
            0 -> {
                sooBlindsList.clear()
                //SOOblindsListAdapter.notifyDataSetChanged()
                stringToToast = getString(R.string.msg_connectToASOO) //Log.i(TAG, "SOO.Blind wants to connect but is not connected")
            }
            1 -> m_myComm.sendCommand(DATA_SOO_BLIND)
            2 -> m_myWifiComm.sendCommand(DATA_SOO_BLIND)
        }
        Toast.makeText(this, stringToToast, Toast.LENGTH_SHORT).show()
    }

    override fun onDestroy() {
        super.onDestroy()
        activityStarted = false
    }

    // adapter class that returns the SOO blinds listview
    inner class SOOblindArrayAdapter(var mCtx: Context, var resource: Int, var items: List<SOOBlindModel>):
        ArrayAdapter<SOOBlindModel>(mCtx, resource, items) {
        override fun getCount(): Int {
            return items.size
        }
        // get item id position in the listview
        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        // return the view with data
        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val layoutInflater: LayoutInflater = LayoutInflater.from(mCtx)

            val sooBlindsView: View = layoutInflater.inflate(resource, null)
            //val imageView: ImageView = sooBlindsView.findViewById(R.id.name_textView)
            var blindName_textView: TextView = sooBlindsView.findViewById(R.id.blindName_textView)
            var blindState_textview: TextView = sooBlindsView.findViewById(R.id.blindState_textview)
            var blindPercent_textview: TextView = sooBlindsView.findViewById(R.id.blindPercent_textview)
            var blind_progressbar: ProgressBar = sooBlindsView.findViewById(R.id.blind_progressbar)

            //var synOutdoor_radioBtn: RadioButton = sooBlindsView.findViewById(R.id.synoutdoor_radioBtn)
            var blind_synOutdoorLyt: LinearLayout = sooBlindsView.findViewById(R.id.blind_synOutdoorLyt)
            var blind_synoutdoor_lowlight: TextView = sooBlindsView.findViewById(R.id.blind_synoutdoor_lowlight)
            var blind_synoutdoor_stronglight: TextView = sooBlindsView.findViewById(R.id.blind_synoutdoor_stronglight)
            var blind_synoutdoor_strongwind: TextView = sooBlindsView.findViewById(R.id.blind_synoutdoor_strongwind)

            var sooBlind: SOOBlindModel = items[position]

            //imageView.setImageDrawable(mCtx.resources.getDrawable(nnnn.photo))
            blindName_textView.text = sooBlind.name /*+ " ID " + sooBlind.id*/
            blindState_textview.text = mCtx.resources.getStringArray(R.array.sooBlindState_array)[sooBlind.getStateSup()]
            blindPercent_textview.text = sooBlind.blindphysposition.toString() + " %"
            blind_progressbar.progress = sooBlind.blindphysposition

            if (syn_sooOutdoor_on) {
                blind_synOutdoorLyt.visibility = View.VISIBLE
                blind_synoutdoor_lowlight.text = sooBlind.lowLight.toString()
                blind_synoutdoor_stronglight.text = sooBlind.strongLight.toString()
                blind_synoutdoor_strongwind.text = sooBlind.strongWind.toString()
            } else {
                blind_synOutdoorLyt.visibility = View.INVISIBLE
            }
            return sooBlindsView
        }
    }

    // enable or disable childs of a viewgroup
    private fun disableEnableControls(enable: Boolean, vg: ViewGroup) {
        for (i in 0 until vg.childCount) {
            val child = vg.getChildAt(i)
            //if(!(enable && syn_sooOutdoor_on && child.tag == "syn_outdoor"))
            /*if(!(enable && syn_sooOutdoor_on && child.tag == "syn_outdoor"))
                child.isEnabled = enable*/
            if (child is ViewGroup) {
                disableEnableControls(enable, child)
            }
        }
    }

    class RecvSOOBlindData() : AsyncTask<Void, Void, String>() {
        override fun doInBackground(vararg params: Void?): String? {

            Log.i(TAG, "Rcvd SOO Blind data")

            /*
            BT Packet header:
            0: Packet type (0 = beacon, 1 = data)
            1-16: ME SPID
            BT packet payload:
            17: Type (do not care)
            Base offset of the first SOO.blind descriptor: 18
            SOO.blind desc...
                16: Requested blind position
                17: Real blind position
                20-23: Requested blind state (enum: 0 - 2)
                24-27: Real blind state (enum: 0 - 2)
                28: Calibration in progress
                29: Calibration done
                32-35: Strong light threshold
                36-39: Low light threshold
                40-43: Strong wind threshold
            Base offset of the first ecostab descriptor: 310 (SOO.blind offset + 6 * size of SOO.blind desc)...
                0-3: Active flag
                20-35: Smart Object name
                36-52: Smart Object AgencyUID
            */

            // Add a sooblind in the list
            fun addSOOblindToList(sooBlindtoAdd : SOOBlindModel){
                sooBlindtoAdd.addPropertyChangeListener(
                    PropertyChangeListener { event ->
                        Log.i(TAG, "Property [${event.propertyName}] changed " + "from [${event.oldValue}] to [${event.newValue}]")
                        //SOOblindsListAdapter.notifyDataSetChanged()
                        //updatedDataSet = true
                        SOOBlindsActivity.hasToNotifyDataSet = true

                        if(sooBlindtoAdd.id == currentSelectedSOOBlind.id) {
                            SOOBlindsActivity.hasToSetSOODetails = true
                        }

                        try {
                            when(CONNEXIONSTATE){
                                1 -> m_myComm.sendSOOBlind(sooBlindtoAdd, event)
                                2 -> m_myWifiComm.sendSOOBlind(sooBlindtoAdd, event)
                            }

                        } catch (e: IOException) {
                            Log.e(com.soo.domotics.TAG,"SOO.Blind couldn't send changes")
                            e.printStackTrace()
                        }
                    }
                )
                sooBlindsList.add(sooBlindtoAdd)
                //sooblindsList.sortBy { it.id }
            }

            // remove a sooblind from the list
            fun removeSOOblindFromList(tmpID : Int){

                sooBlindsList.remove(sooBlindsList.first{it.id == tmpID})
                //SOOblindsListAdapter.notifyDataSetChanged()
                //SOOBlindsActivity.updatedDataSet = true
                SOOBlindsActivity.hasToNotifyDataSet = true
                if(tmpID == currentSelectedSOOBlind.id) // disable rightmenulyt?
                    SOOBlindsActivity.hasToSetSOODetails = true
            }

            var inputData = m_sooBlindData
            var descPtr = m_myComm.pktPtr + m_myComm.headerPtr
            val descPtrLength = 48 // todo stocker mieux
            var ecostabPtr = descPtr + 292 // todo stocker mieux
            // 292 = ServerSOOblindsListSize * 48 (blind descriptors) + 4 (my_id)
            val ecostabPtrLength = descPtrLength + 4 //= 52 // todo stocker mieux
            var ServerSOOblindsListSize = 6 // = 6

            // This condition avoid old packages treatment // TODO: in the agency, create a general age value so we can check it directly when receiving the data in the CommManager, and not there
            if(ByteBuffer.wrap(inputData.copyOfRange(ecostabPtr + 4, ecostabPtr + 8)).order(ByteOrder.LITTLE_ENDIAN).getInt() > DescrAge)
            {
                DescrAge = ByteBuffer.wrap(inputData.copyOfRange(ecostabPtr + 4, ecostabPtr + 8)).order(ByteOrder.LITTLE_ENDIAN).getInt()

                for(i in 0..ServerSOOblindsListSize - 1) { // todo clean everything (uniformisation of integers in int, compare directly boolean (why isActive would be int?), ...
                    val isActiveInt = Integer(ByteBuffer.wrap(inputData.copyOfRange(ecostabPtr, ecostabPtr + 4)).order(ByteOrder.LITTLE_ENDIAN).getInt())

                    if(sooBlindsList.any{it.id == i}) {
                        if(isActiveInt.compareTo(1) != 0)
                            removeSOOblindFromList(i) //sooblindsList.remove(sooblindsList.first{it.id == i})
                        else{
                            sooBlindsList.first { it.id == i }.blindnextposition = inputData[descPtr + 16].toInt()
                            sooBlindsList.first { it.id == i }.blindphysposition = inputData[descPtr + 17].toInt()
                            //sooBlindsList.first { it.id == i }.state = ByteBuffer.wrap(inputData.copyOfRange(descPtr + 20, descPtr + 24)).order(ByteOrder.LITTLE_ENDIAN).getInt()
                            sooBlindsList.first { it.id == i }.state = ByteBuffer.wrap(inputData.copyOfRange(descPtr + 24, descPtr + 28)).order(ByteOrder.LITTLE_ENDIAN).getInt()
                            sooBlindsList.first { it.id == i }.strongLight = ByteBuffer.wrap(inputData.copyOfRange(descPtr + 32, descPtr + 36)).order(ByteOrder.LITTLE_ENDIAN).getInt()
                            sooBlindsList.first { it.id == i }.lowLight = ByteBuffer.wrap(inputData.copyOfRange(descPtr + 36, descPtr + 40)).order(ByteOrder.LITTLE_ENDIAN).getInt()
                            sooBlindsList.first { it.id == i }.strongWind = ByteBuffer.wrap(inputData.copyOfRange(descPtr + 40, descPtr + 44)).order(ByteOrder.LITTLE_ENDIAN).getInt()
                            sooBlindsList.first { it.id == i }.calibrating = (1 == inputData[descPtr + 28].toInt())
                            sooBlindsList.first { it.id == i }.calibrationDone = (1 == inputData[descPtr + 29].toInt())

                            /*sooblindsList[i].blindnextposition = inputData[descPtr + 16].toInt()
                            sooblindsList[i].blindphysposition = inputData[descPtr + 17].toInt()
                            sooblindsList[i].state = ByteBuffer.wrap(inputData.copyOfRange(descPtr + 20, descPtr + 24)).order(ByteOrder.LITTLE_ENDIAN).getInt()
                            sooblindsList[i].strongLight = ByteBuffer.wrap(inputData.copyOfRange(descPtr + 32, descPtr + 36)).order(ByteOrder.LITTLE_ENDIAN).getInt()
                            sooblindsList[i].lowLight = ByteBuffer.wrap(inputData.copyOfRange(descPtr + 36, descPtr + 40)).order(ByteOrder.LITTLE_ENDIAN).getInt()
                            sooblindsList[i].strongWind = ByteBuffer.wrap(inputData.copyOfRange(descPtr + 40, descPtr + 44)).order(ByteOrder.LITTLE_ENDIAN).getInt()
                            */

                            sooBlindsList.first { it.id == i }.active = isActiveInt.compareTo(1) == 0
                            sooBlindsList.first { it.id == i }.name = String(inputData.copyOfRange(ecostabPtr + 20, ecostabPtr + 36))
                            //sooblindsList.first{it.id == i}.agencyUID = inputData.copyOfRange(ecostabPtr + 36, ecostabPtr + 52)

                            if(sooBlindsList.first{it.id == i}.id == currentSelectedSOOBlind.id) {
                                //rightMenFresh = true
                                //hasToUpdateRightMenuValues = true
                                SOOBlindsActivity.hasToSetSOODetails = true
                            }

                            //Log.i(TAG, "Descr age = "+ByteBuffer.wrap(inputData.copyOfRange(ecostabPtr + 4, ecostabPtr + 8)).order(ByteOrder.LITTLE_ENDIAN).getInt())
                            //Log.i(TAG, inputData.copyOfRange(ecostabPtr, ecostabPtr + 8).joinToString(" ") { String.format("%02X", (it.toInt() and 0xff))})
                        }
                    }
                    else if(isActiveInt.compareTo(1) == 0)
                        addSOOblindToList(SOOBlindModel(i, "", 0))

                    descPtr += descPtrLength
                    ecostabPtr += ecostabPtrLength
                }
            }
            return null
        }
    }

    companion object {
        lateinit var SOOblindsListAdapter : SOOblindArrayAdapter
        private var sooBlindsList = mutableListOf<SOOBlindModel>()// GENERAL LIST OF SOO.BLINDS
        private var currentSelectedSOOBlind = SOOBlindModel(-1, "-", -1)
        private var syn_sooOutdoor_on : Boolean = false // SOO.outdoor synergy layer activated

        var m_sooBlindData = m_myComm.genData(DATA_SOO_BLIND)

        var activityStarted = false

        var hasToNotifyDataSet = false
        var hasToSetSOODetails = false

        var notifyAdapterThreadStarted = false
        var DescrAge = 0 // age of descriptors
    }
}